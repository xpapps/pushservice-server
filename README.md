A mobile (iOS, Android) push notification handler for the server.
 
 ## Basic usage:
  * load module
  * call the setup() function with the required SNS configurations for each platform
 
 ### Example:
 ```javascript
 var android = {
     region: 'eu-west-1',
     apiVersion: '2010-03-31',
     accessKeyId: 'accessKeyId',
     secretAccessKey: 'secretAccessKey',
     platformApplicationArn: 'platformApplicationArn'
 };

 var ios = {
     region: 'eu-west-1',
     apiVersion: '2010-03-31',
     accessKeyId: 'accessKeyId',
     secretAccessKey: 'secretAccessKey',
     platformApplicationArn: 'platformApplicationArn',
     sandbox: false // Should be true in development
 };
 
 pushService.setup(ios, android); // You can add a third boolean parameter to display server logs
 ```
