/*
 * A mobile (iOS, Android) push notification handler for the server.
 *
 * Usage: load module, then call setApps function with the required SNS configurations
 *
 * Example:

 *   var android = {
 *        region: 'eu-west-1',
 *        apiVersion: '2010-03-31',
 *        accessKeyId: 'accessKeyId',
 *        secretAccessKey: 'secretAccessKey',
 *        platformApplicationArn: 'platformApplicationArn'
 *    };

 *    var ios = {
 *        region: 'eu-west-1',
 *        apiVersion: '2010-03-31',
 *        accessKeyId: 'accessKeyId',
 *        secretAccessKey: 'secretAccessKey',
 *        platformApplicationArn: 'platformApplicationArn',
 *        sandbox: false // Should be true in development
 *    };
 *
 *    pushService.setup(ios, android); // You can add a third boolean parameter to display server logs
 *
 */

var SNS = require('sns-mobile');
var EVENTS = SNS.EVENTS;
var Q = require('q');
var log = require('../logentries').getInstance();
var extend = require('extend');

var hideLogs = false;
function print(msg) {
    if (!hideLogs) {
        log.info(msg);
    }
}

print('pushService finished loading dependencies');

var androidApp;
var iosApp;

function setup(ios, android, debugging) {
    if (!iosApp && !androidApp) {
        iosApp = new SNS({
            platform: SNS.SUPPORTED_PLATFORMS.ANDROID,
            region: ios.region,
            apiVersion: (ios.version || '2010-03-31'),
            accessKeyId: ios.accessKeyId,
            secretAccessKey: ios.secretAccessKey,
            platformApplicationArn: ios.platformApplicationArn
        });

        androidApp = new SNS({
            platform: SNS.SUPPORTED_PLATFORMS.ANDROID,
            region: android.region,
            apiVersion: (android.version || '2010-03-31'),
            accessKeyId: android.accessKeyId,
            secretAccessKey: android.secretAccessKey,
            platformApplicationArn: android.platformApplicationArn,
            sandbox: ios.sandbox // Should be true in development
        });
    }

    if (debugging) {
        hideLogs = true;
    }

}

function subsribeUserToSNS(device, uuid) {
    var promise = Q.defer();

    // Add a user, the endpointArn is their unique id
    // endpointArn is required to send messages to the device

    var app = (device.toUpperCase() == "IOS" ? iosApp : androidApp);

    app.addUser(uuid, JSON.stringify({
        registered: 'true'
    }), function (err, endpointArn) {
        if (err) {
            throw err;
        }
        promise.resolve(endpointArn);
    });

    print('Register user phone to SNS, device: ' + device + ', uuid: ' + uuid);

    return promise.promise;
}


function unregister(user) {
    var deferred = Q.defer();

    user.endpointArn = user.device = null;
    user.save(function (err, user) {
        if (err) deferred.reject();
        deferred.resolve();
    });
    if (user)
        print('Unregistered user phone from SNS, user: ' + user._id);
    return deferred.promise;
}

function sendMessageToDevice(endpointArn, title, subject, device, _options) {
    var options = extend(true, {}, _options, {
        content: {},
        imgName: null,
        badgeNumber: null,
        alertSettings: {sound: true, vibrate: true}
    });

    print('pushService.sendMessageToDevice: alertSettings = ' + options.alertSettings + ' endpointArn = ' + endpointArn
        + ' subject ' + subject + ' device = ' + device + ' title = ' + title + ' imgName = ' + options.imgName + ' badgeNumber = ' +
        options.badgeNumber);

    var promise = Q.defer();

    if (!device || device == 'undefined' || !endpointArn || endpointArn == 'undefined')
        return;


    if (device.toUpperCase() == "IOS")
        return sendIOSMessage(options.alertSettings, endpointArn, subject, options.content, title, options.imgName, options.badgeNumber);
    else
        return sendAndroidMessage(options.alertSettings, endpointArn, subject, options.content, title, options.imgName, options.badgeNumber);

}

function sendIOSMessage(alertSettings, endpointArn, subject, _content, title, imgName, badgeNumber) {

    var promise = Q.defer();
    var sound = alertSettings.sound ? ",\"sound\":\"incoming.aiff\"" : "";
    var badge = ",\"badge\": " + (badgeNumber + 1);

    var messageKey = iosApp.sandbox ? 'APNS_SANDBOX' : 'APNS';

    var notification = {
        aps: {
            alert: {
                sentNotificationId: '',
                title: title,
                body: subject,
                s: alertSettings.sound
            }
        }
    };

    // Add badge and sound options:
    if (alertSettings.sound) {
        notification.aps.sound = "incoming.aiff";
    }
    if (alertSettings.badge) {
        notification.aps.badge = (badgeNumber + 1);
    }

    if (_content) {
        notification.content = _content;
    }


    notification = JSON.stringify(notification);

    var message = {};
    message[messageKey] = notification;

    print('pushService.sendIOSMessage: message = ' + message);

    iosApp.sendMessage(endpointArn, message, function (err, messageId) {
        if (err) {
            log.err('pushService.sendIOSMessage: message = ' + message + ' error = ' + err);
            promise.reject(err);
            return;
        }
        promise.resolve(messageId);
    });
    return promise.promise;
}

function sendAndroidMessage(alertSettings, endpointArn, subject, _content, title, imgName, badgeNumber) {

    var promise = Q.defer();

    // var badge = "\"badgeNumber\":\"" + (badgeNumber + 1) + "\",";
    // var sound = "\"s\":\"" + alertSettings.sound + "\",";
    // var vibrate = "\"v\":\"" + alertSettings.vibrate + "\",";

    
    var message = {
        delay_while_idle: false,
        collapse_key: 'SNS test',
        data: content, notification: {title: title, body: subject}, time_to_live: 125, dry_run: false
    }


    if (imgName) {
        message.GCM.notification.icon = imgName;
    }

    print('pushService.sendAndroidMessage: message = ' + message);
    var content = JSON.stringify(_content).replace(/"/g,"\"");

    var message = {"GCM": "{\"delay_while_idle\":false,\"collapse_key\":\"SNS test\",\"notification\":{\"title\":\"" + title + "\",\"body\":\"" + subject + "\"},\"data\":" + content + ",\"time_to_live\":125,\"dry_run\":false}"};

    androidApp.sendMessage(endpointArn, message, function (err, messageId) {
        if (err) {
            log.err('pushService.sendAndroidMessage: message = ' + message + ' error = ' + err);
            promise.reject(err);
            return;
        }
        promise.resolve(messageId);
    });
    return promise.promise;
}

exports.setup = setup;
exports.unregister = unregister;
exports.subsribeUserToSNS = subsribeUserToSNS;
exports.sendMessageToDevice = sendMessageToDevice;


